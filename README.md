# flutter_login_screen

[![Codemagic build status](https://api.codemagic.io/apps/5c95ecb46023d00017ec0b94/5c95ecb46023d00017ec0b93/status_badge.svg)](https://codemagic.io/apps/5c95ecb46023d00017ec0b94/5c95ecb46023d00017ec0b93/latest_build)

A new Flutter application. 
The star point was this design:
[https://dribbble.com/shots/6192981-Affiliate-App-Login-Interaction/attachments/1327477]


## Getting Started
### Resources :
- [How to make a clip path for the curve](https://iirokrankka.com/2017/09/04/clipping-widgets-with-bezier-curves-in-flutter/)
- [How to make the float Button Animation](https://stackoverflow.com/questions/45539395/flutter-float-action-button-hiding-the-visibility-of-items#45598028)

### Final app

[APK](media/app.apk)

[Video](media/login.mp4)

![](./media/Screenshot01.png "")

![](./media/Screenshot02.png "")

![](./media/Screenshot03.png "")

![](./media/Screenshot04.png "")

![](./media/Screenshot05.png "")

![](./media/Screenshot06.png "")


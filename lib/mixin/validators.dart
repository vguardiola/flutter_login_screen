import 'dart:async';

class Validators {
  final isEmailCorrect = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
      if (email.isNotEmpty) {
        final RegExp regex = new RegExp(
            r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
        if (regex.hasMatch(email)) {
          sink.add(email);
        } else {
          sink.addError('Email Invalid');
        }
      }
    },
  );

  final isPasswordCorrect = StreamTransformer<String, String>.fromHandlers(
    handleData: (password, sink) {
      final RegExp regexNum = new RegExp(r"\d");
      final RegExp regexUpperCase = new RegExp(r"[A-Z]");
      final RegExp regexSpecial = new RegExp(r"[!@$%&^*\(\)]");
      String error = '';
      if (password.length < 8) {
        error += ' - Minimun 8 charaters\n';
      }
      if (false == regexNum.hasMatch(password)) {
        error += ' - Minimun 1 number\n';
      }
      if (false == regexUpperCase.hasMatch(password)) {
        error += ' - Minimun 1 Uppper Case\n';
      }
      if (false == regexSpecial.hasMatch(password)) {
        error += ' - Minimun 1 special char\n';
      }

      if (error == '') {
        sink.add(password);
      } else {
        sink.addError(error);
      }
    },
  );
}

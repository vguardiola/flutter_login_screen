import 'package:dribblelogin/bloc/login_bloc.dart';
import 'package:dribblelogin/bloc/login_bloc_provider.dart';
import 'package:flutter/material.dart';

class EmailField extends StatefulWidget {
  @override
  _EmailFieldState createState() => _EmailFieldState();
}

class _EmailFieldState extends State<EmailField> {
  final Color greyLight = Color.fromRGBO(88, 92, 99, 1);
  @override
  Widget build(BuildContext context) {
    LoginBloc bloc = LoginBlocProvider.of(context);
    return Container(
      color: Color.fromRGBO(45, 51, 60, 1),
      padding: const EdgeInsets.all(8.0),
      child: StreamBuilder<Object>(
        stream: bloc.email,
        builder: (context, snapshot) {
          return TextField(
            style: TextStyle(color: greyLight),
            decoration: InputDecoration(
              hintText: 'Email',
              hintStyle: TextStyle(color: greyLight),
              border: InputBorder.none,
              errorText: snapshot.error,
              errorMaxLines: 1,
              errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
              icon: Icon(
                Icons.person,
                color: greyLight,
              ),
            ),
            onChanged: bloc.validateEmail,
            keyboardType: TextInputType.emailAddress,
            maxLines: 1,
          );
        },
      ),
    );
  }
}

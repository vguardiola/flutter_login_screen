import 'package:dribblelogin/bloc/login_bloc.dart';
import 'package:dribblelogin/bloc/login_bloc_provider.dart';
import 'package:flutter/material.dart';

class PasswordField extends StatefulWidget {
  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _obscurePassword;
  final Color greyLight = Color.fromRGBO(88, 92, 99, 1);

  @override
  void initState() {
    super.initState();
    _obscurePassword = true;
  }

  @override
  Widget build(BuildContext context) {
    LoginBloc bloc = LoginBlocProvider.of(context);
    return Container(
      color: Color.fromRGBO(45, 51, 60, 1),
      padding: const EdgeInsets.all(12.0),
      child: StreamBuilder<Object>(
        stream: bloc.password,
        builder: (context, snapshot) {
          return TextField(
            style: TextStyle(color: greyLight),
            obscureText: _obscurePassword,
            decoration: InputDecoration(
              hintText: 'Password',
              fillColor: Colors.red,
              hintStyle: TextStyle(color: greyLight),
              border: InputBorder.none,
              icon: Icon(
                Icons.lock,
                color: greyLight,
              ),
              suffixIcon: GestureDetector(
                child: Icon(
                  Icons.remove_red_eye,
                  color: greyLight,
                ),
                onLongPress: _showPassword,
                onLongPressUp: _hidePassword,
              ),
              errorText: snapshot.error,
              errorMaxLines: 4,
              errorBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
            ),
            onChanged: bloc.validatePassword,
          );
        },
      ),
    );
  }

  void _showPassword() {
    setState(() {
      _obscurePassword = false;
    });
  }

  void _hidePassword() {
    setState(() {
      _obscurePassword = true;
    });
  }
}

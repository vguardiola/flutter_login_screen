import 'package:flutter/material.dart';

class FloatButton extends StatefulWidget {
  final Color color;
  final GlobalKey<ScaffoldState> scaffoldKey;

  const FloatButton({Key key, this.color, this.scaffoldKey}) : super(key: key);

  @override
  _FloatButtonState createState() => _FloatButtonState();
}

class _FloatButtonState extends State<FloatButton> with TickerProviderStateMixin {
  bool _isRotated = false;
  int _angle = 90;
  AnimationController _controller;
  Animation<double> _animation1;
  Animation<double> _animation2;
  Animation<double> _animation3;

  @override
  void initState() {
    _controller = new AnimationController(vsync: this, duration: const Duration(milliseconds: 180));
    _animation1 = new CurvedAnimation(parent: _controller, curve: new Interval(0.0, 1.0, curve: Curves.linear));
    _animation2 = new CurvedAnimation(parent: _controller, curve: new Interval(0.5, 1.0, curve: Curves.linear));
    _animation3 = new CurvedAnimation(parent: _controller, curve: new Interval(0.8, 1.0, curve: Curves.linear));
    _controller.reverse();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return new Stack(
      children: <Widget>[
        _isRotated == true
            ? Positioned(
                top: 0,
                left: 0,
                width: _size.width,
                height: _size.height,
                child: GestureDetector(
                  onTap: _rotate,
                  child: Material(
                    elevation: 10,
                    color: Colors.black.withOpacity(0.25),
                  ),
                ),
              )
            : Positioned(
                top: 0,
                left: 0,
                width: 0,
                height: 0,
                child: SizedBox(height: 0.0, width: 0.0),
              ),
        new Positioned(
          bottom: 64,
          left: _size.width / 2 - 80,
          child: new Container(
            child: new ScaleTransition(
              scale: _animation3,
              alignment: FractionalOffset.center,
              child: new Material(
                color: Colors.black,
                type: MaterialType.circle,
                elevation: 6.0,
                child: new InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    if (_isRotated) {
                      showMessage('Create a new Account');
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Icon(
                      Icons.add,
                      color: Colors.white,
                      semanticLabel: 'Add new account',
                      key: Key('newAccount'),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        new Positioned(
          bottom: 88.0,
          left: _size.width / 2 - 20,
          child: new Container(
            child: new ScaleTransition(
              scale: _animation2,
              alignment: FractionalOffset.center,
              child: new Material(
                color: Colors.black,
                type: MaterialType.circle,
                elevation: 6.0,
                child: new InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    if (_isRotated) {
                      showMessage('Login with Facebook Account');
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Icon(
                      Icons.face,
                      color: Colors.white,
                      size: 24,
                      key: Key('loginFacebook'),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        new Positioned(
          bottom: 64.0,
          left: _size.width / 2 + 34,
          child: new Container(
            child: new ScaleTransition(
              scale: _animation1,
              alignment: FractionalOffset.center,
              child: new Material(
                color: Colors.black,
                type: MaterialType.circle,
                elevation: 6.0,
                child: new InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    if (_isRotated) {
                      showMessage('Login with Google Account');
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Icon(
                      Icons.adb,
                      color: Colors.white,
                      size: 24,
                      key: Key('loginGoogle'),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        new Positioned(
          top: _size.height - 72,
          left: _size.width / 2 - 28,
          child: new Material(
            color: _isRotated == true ? Colors.pinkAccent : widget.color,
            type: MaterialType.circle,
            elevation: 6.0,
            child: InkWell(
              onTap: _rotate,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: new RotationTransition(
                  turns: new AlwaysStoppedAnimation(_angle / 360),
                  child: new Icon(
                    Icons.add,
                    color: Colors.white,
                    semanticLabel: 'View more options',
                    key: Key('moreOptions'),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _rotate() {
    setState(() {
      if (!_isRotated) {
        _angle = 45;
        _controller.forward();
      } else {
        _angle = 90;
        _controller.reverse();
      }
      _isRotated = !_isRotated;
    });
  }

  void showMessage(String message) {
    widget.scaffoldKey.currentState.showSnackBar(
      new SnackBar(
        backgroundColor: widget.color,
        content: new Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }
}

import 'dart:async';

import '../mixin/validators.dart';

class LoginBloc extends Object with Validators {
  final _email = StreamController<String>.broadcast();
  final _password = StreamController<String>.broadcast();

  Stream<String> get email => _email.stream.transform(isEmailCorrect);

  Stream<String> get password => _password.stream.transform(isPasswordCorrect);

  Function(String) get validateEmail => _email.sink.add;

  Function(String) get validatePassword => _password.sink.add;

  dispose() {
    _email.close();
    _password.close();
  }
}

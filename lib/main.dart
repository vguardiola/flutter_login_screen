import 'package:dribblelogin/bloc/login_bloc_provider.dart';
import 'package:dribblelogin/widgets/email_field.dart';
import 'package:dribblelogin/widgets/float_button.dart';
import 'package:dribblelogin/widgets/password_field.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'opensans',
      ),
      home: LoginPage(title: 'Affiliate Login Page'),
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  final Color _bgColor = Color.fromRGBO(34, 40, 49, 1);
  final Color _blueColor = Color.fromRGBO(42, 98, 204, 1);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Animation<double> _slideAnimation;
  Animation<double> _imageAnimation;
  Animation<double> _textOpacityAnimation;
  AnimationController _separatorAnimationController;
  AnimationController _opacityAnimationController;
  bool _isUp;
  bool _showDownText;

  final double _initialBeginSlide = 0.77;

  @override
  void initState() {
    super.initState();
    _isUp = false;
    _showDownText = true;
  }

  @override
  void dispose() {
    if (null != _separatorAnimationController) {
      _separatorAnimationController.dispose();
    }
    if (null != _opacityAnimationController) {
      _opacityAnimationController.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return LoginBlocProvider(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: _bgColor,
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                Container(
                  height: _size.height * (_slideAnimation != null ? _slideAnimation.value : _initialBeginSlide),
                  width: _size.width,
                  color: _blueColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 50,
                      ),
                      _isUp == true
                          ? Text(
                              'Welcome to\nAffiliate system',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                              textAlign: TextAlign.center,
                            )
                          : Container(height: 0.0),
                      Image.asset(
                        'assets/img/affiliate.png',
                        width: _size.width * (_slideAnimation != null ? _imageAnimation.value : 0.77),
                      ),
                      Opacity(
                        opacity: _textOpacityAnimation != null ? _textOpacityAnimation.value : 1,
                        child: _showDownText == true
                            ? Text(
                                'We Care About\nYour Profit',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.center,
                              )
                            : Container(height: 0.0),
                      ),
                      Opacity(
                        opacity: _textOpacityAnimation != null ? _textOpacityAnimation.value : 1,
                        child: _showDownText == true
                            ? Padding(
                                padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
                                child: Text(
                                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed egestas condimentum orci in finibus. Sed.',
                                  style: TextStyle(
                                    color: Color.fromRGBO(174, 192, 227, 1),
                                    fontSize: 14,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            : SizedBox(height: 0.0),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: _animateToUpOrDown,
                  child: ClipPath(
                    child: Container(
                      height: 80.0,
                      width: _size.width,
                      color: _blueColor,
                    ),
                    clipper: BottomWaveClipper(),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      width: _size.width,
                      child: GestureDetector(
                        child: Text(
                          'Login To Your Account',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        onTap: _animateToUpOrDown,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: _isUp == true
                          ? Container(
                              child: Column(
                                children: <Widget>[
                                  EmailField(),
                                  SizedBox(height: 8.0),
                                  PasswordField(),
                                ],
                              ),
                            )
                          : Container(
                              color: Color.fromRGBO(45, 51, 60, 1),
                            ),
                    ),
                  ],
                ),
              ],
            ),
            _isUp == true ? FloatButton(color: _blueColor, scaffoldKey: _scaffoldKey) : SizedBox(height: 0.0),
          ],
        ),
      ),
    );
  }

  void _animateToUpOrDown() {
    _separatorAnimationController = new AnimationController(duration: new Duration(milliseconds: 1000), vsync: this);
    double _beginSlide = _initialBeginSlide;
    double _endSlide = 0.45;
    double _beginImage = 0.77;
    double _endImage = 0.50;
    double _beginOpacity = 1;
    double _endOpacity = 0;
    int _opacityDuration = 500;
    if (_isUp == true) {
      _beginSlide = 0.45;
      _endSlide = _initialBeginSlide;
      _beginImage = 0.50;
      _endImage = 0.77;
      _beginOpacity = 0;
      _endOpacity = 1;
      _opacityDuration = 500;
    }
    _opacityAnimationController =
        new AnimationController(duration: new Duration(milliseconds: _opacityDuration), vsync: this);

    _slideAnimation = Tween<double>(
      begin: _beginSlide,
      end: _endSlide,
    ).animate(_separatorAnimationController);
    _slideAnimation.addListener(() => this.setState(() {
          if (_slideAnimation.isCompleted) {
            _isUp = !_isUp;
          }
        }));
    _imageAnimation = Tween<double>(
      begin: _beginImage,
      end: _endImage,
    ).animate(_separatorAnimationController);
    _textOpacityAnimation = Tween<double>(
      begin: _beginOpacity,
      end: _endOpacity,
    ).animate(_opacityAnimationController);
    _textOpacityAnimation.addListener(() => this.setState(() {
          if (_textOpacityAnimation.isCompleted) {
            _showDownText = !_showDownText;
          }
        }));
    _separatorAnimationController.forward();
    _opacityAnimationController.forward();
  }
}

class BottomWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, 0.0);
    var firstControlPoint = Offset(20, 40);
    var firstEndPoint = Offset(size.width / 4, 40);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy);
    path.relativeLineTo(size.width / 2, 0);
    var secondControlPoint = Offset(size.width - (size.width / 16) + 10, 40);
    var secondEndPoint = Offset(size.width, size.height);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, 0.0);
    path.lineTo(0.0, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

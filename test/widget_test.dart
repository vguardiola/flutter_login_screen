// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:dribblelogin/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Test Inital Page', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    expect(find.text('We Care About\nYour Profit'), findsOneWidget);
    expect(find.text('Login To Your Account'), findsOneWidget);
    expect(find.text('Welcome to\nAffiliate system'), findsNothing);
  });

  testWidgets('Test Tap Slide', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp());
    await tester.tap(find.text('Login To Your Account'));
    await tester.pumpAndSettle();
    expect(find.text('We Care About\nYour Profit'), findsNothing);
    expect(find.text('Login To Your Account'), findsOneWidget);
    expect(find.text('Welcome to\nAffiliate system'), findsOneWidget);
    expect(find.byKey(Key('moreOptions')), findsOneWidget);
    await tester.tap(find.byKey(Key('moreOptions')));
    await tester.pumpAndSettle();
  });
}
